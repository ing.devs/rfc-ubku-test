# RFC Wallet+BlockChain
## _Made For UBankU_
by David E. Vargas S

1/03/2022

![Image](/images/UbankuLogo.png "Logo UBanku")


## TL;DR

- Aprovechar los procesos de identificación de las blockchain privadas para diferenciar cada actor de la Wallet, permitiendo así tener control sobre quienes la utilizaran (usuarios, proveedores de servicios, etc) . 
- Se propone que cada usuario/proveedor/servicio sea una entidad contra la que se generaría una transacción mediante flujos independientes. 

## Motivación
¿Qué motiva esta decisión y por qué es importante?
> 1. Tecnología cambiante y nuevas aplicaciones en el mundo financiero para tener historial transaccional inmutable y que cada usuario/servicio sea un nodo independiente
> 2. Seríamos revolucionarios en la industria con la 1ra App Colombiana con este tipo de tecnología
> 3. Atraería más inversionistas para una siguiente ronda por la innovación y concepto que se va a implementar, sería un factor diferencial en la industria.

## Propuesta de implementación

- Generar un sistema para dar de alta usuarios/proveedores/servicios..etc en una blockchain privada 
- Cada actor es un nodo dentro del ecosistema que se indentifica univocamente
- Existen nodos autorizadores o de control para autorizar/firmar todas las transacciones que se realicen
- Cada transacción realizada en el sistema debe tener información de tiempo, términos, cláusulas y demás, que cada parte debe aceptar previo a la "firma" de la transacción

##### Flujo de la Wallet
>- General
 	![Image](./images/General.png "General")
>- Registro
![Image](./images/Registro.png "General")
>- Login
![Image](./images/Login.png "Login")
>- Main Screen
![Image](./images/Main.png "MainScreen")
>- Utilización
![Image](./images/Use.png "Utilización")


##### Links de interés

+ [Código Ejemplo P2P-Lending](https://github.com/mradkov/p2p-lending)
+ [Empresa Real Lending](https://lendoit.com/)


## Métricas
Que métricas debemos vamos a instrumentar, o monitorear para observar las implicaciónes de esta decisiòn?
- Por ahora colocaría metricas para hacer una prueba de concepto y luego veríamos las de producción, es mejor probar antes cada concepto y el avance seria poner criterios de aceptación a cada prueba de los siguientes temas:
> 1. Flujos basados en blockchain
> 2. Rastreo de transacciones por usuario, servicio, registros de tiempo etc
> 3. Validaciones y darse de alta basado en nodos autorizadores (Red privada)

## Riesgos e inconvenientes.
¿Hay razones por las que no deberiamos hacer esto?
- Tecnología muy nueva para manejo de flujos
- Readiness de los aliados para manejar estos sistemas
- Entidades o nodos autorizadores son quienes mantienen el sistema y es la piedra angular
- Pocos participantes-> Mayor probabilidades de ataques

## Alternativas
¿Hay otras formas de resolver éste problema?
- Generar una "organización" dentro de la red o consorcio lo suficientemente robusto para mantener la seguridad del sistema
- Cambiar a un esquema de servicios embebidos financieros por APIs y el "flujo del funcionamiento" si enfocarlo solo con Blockchain (Podria ser fase1) y luego ir implementando más conceptos

## Impacto potencial y dependencias
¿Qué otros sistemas se verán afectados con esta propuesta?
- Datos se mantienen en bloques cuya minería es mas lenta
- Conocimiento especializado y consecución de personal (entrenamiento en blockchain + dispositivos móviles)

## ¿Qué consideraciones de seguridad debemos tener?
- Prevención ataques al servidor central y nodos autorizadores
- Deberiamos trabajar detras de un firewall robusto ya sea local o a través de por ejemplo AWS WAF y además estar sobre una VPN Privada(Local o Cloud)
- Dominio de los nodos autorizadores mediante sistemas independientes en equipo dedicado de Gobernanza de la información
## ¿Como pueden explotar esta parte del sistema?
- Mediante un ataque cuando tengamos pocos nodos un hacker puede adueñarse del sistema
- Ingenieria social en etapas tempranas

## ¿Qué impacto tiene esta decisión sobre soporte al cliente?
- Para validar las transacciones y dar soporte se debe estar "registrado" como un usuario del sistema, para poder validar la cadena/ caso que se esté revisando y se debería crear un sistema de roles y perfiles para soporte
- Se debe crear un sistema de rastreo sencillo para llegar a las solicitudes ya sea por usuario, por servicio, por fecha en cada transacción y así generar un indice de búsquedas

## Preguntas sin resolver
¿Qué preguntas no hemos resuelto?
- Hacia que tipo de servicios nos vamos a enfocar?
- Cada modelo de negocio o servicio que se monte en la wallet debe trabajarse mediante un flujo independiente o se debería tener uniformidad
- En donde estará alojada la plataforma, nube privada, local, hibrida, para cada caso hay ventajas y desventajas
- Quien rige los modelos de negocio sobre cada servicio, a veces las formas de pagos, cobros, fees etc, son más complejas que los mismos desarrollos y se debe estar alineado
- Cuál es la necesidad principal a resolver en un corto plazo para poder enfocar esfuerzos?
- Que equipo de trabajo y recursos se tienen disponibles


_Metodología a Utilizar_
> Crear 3 células ágiles de trabajo con grupos independientes para: 
> - Flujos variables
> - Rastreo de transacciones
> - Validaciones y darse de alta

> Definición de la funcionalidad general en el backlog de cada célula mediante herramienta como JIRA.

> Planeación de los sprints.

> Generación de historias de usuario con críterios y pruebas de aceptación.

> Definir metodo de seguimiento con equipo de trabajo (por historias de usuario) máx 2-3 semanas por Sprint.

> Trabajo diario de cada célula con su product owner, daily meeting y scrum master. 

 
## MadeFor Ubanku Test Day 1

Eyes Only

**David E Vargas S
1 Marzo 2022**
